# Basic Simple Website
This is a basic website using the ElevenTy static site generator and will be the building blocks for both my ElevenTy SSG baseline and design system baseline.

## CSS/Sass Structure
This folder is the primary directory for all Sass files used within the projet. The files use a modified BEM syntax and use the design system prefix TREX, which ensures, if needed, we can add any 3rd-party system without conflict.

### 00 - Utilities
Gloabally available settings, configuration files, tools, helper files, variables, functions, and mixins. These files are meant to be just helpers which don’t output any CSS when compiled.
                       
* functions
* global settings
* mixins
* variables

### 01 - Settings
Holds the boilerplate code for the project. Including standard styles such as resets, typographic rules, and brand colors which are commonly used throughout the project.

* accessability
* box sizing
* breakpoints
* clear fix
* colors
* font smoothing
* full width
* hidden
* layout - used as the page wrapper
* margin
* reset
* sticky
* typography

### 02 - Elements
Holds unclassed HTML elements. Including standard styles for items such as H1-H6, lists, links, and images used throughout the project.

* button
* figure
* headings
* hr
* icons
* images
* input
* links
* lists
  
### 03 - Objects
Uses OOCSS design patterns. Contains all styles involved with the layout of the project. Such as styles for the header, footer, navigation, grid system, with no cosmetics, and uses agnostic class naming.

* accordion
* breadcrumb
* buttons
* media - used to group different kids of media i.e. images or videos with blocks of text
* menu
* notification
* pagination
* slider
* table

### 04 - Components
Holds all of the styles for buttons, heros, carousels, sliders, and similar page components or widgets using explicite class naming. The project will typically contain a lot of component files — as the whole site/app should be mostly composed of small modules.

* card
* carosel
* form
* hero
* modal
* promo strip
* article
* header
* footer
* main
* navigation
* section
* sidebar

### 05 - Pages
Any styles specific to individual pages will sit here. For example it’s not uncommon for the home page to require page specific styles that no other page receives. These should be renamed to refelect the current project.

* about us
* article
* article list
* contact
* index
* service
* service list
* work
* work list



